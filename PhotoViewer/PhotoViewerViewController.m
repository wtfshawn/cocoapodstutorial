//
//  PhotoViewerViewController.m
//  PhotoViewer
//
//  Created by Shawn Simon on 12/6/2013.
//  Copyright (c) 2013 Shawn Simon. All rights reserved.
//

#import "PhotoViewerViewController.h"
#import "PhotoCollectionView.h"
#import "PhotoCollectionViewCell.h"
#import "PhotoCollectionViewFlowLayout.h"
#import "UIImage+BlurredFrame.h"

@interface PhotoViewerViewController ()

// Properties.
@property (strong, nonatomic) PhotoCollectionView* photoCollectionView;
@property (strong, nonatomic) NSArray* photoFileNameArray;
@property (strong, nonatomic) NSString* sourcePath;
@property (strong, nonatomic) NSMutableArray* photoDates;

// Private methods.
- (void) setupCollectionView;
- (void) loadPictures;

@end

@implementation PhotoViewerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self loadPictures];
    [self setupCollectionView];
}

- (void) loadPictures
{
    // Save our path to the photos.
    self.sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Pictures"];
    // Save photo names in photoArray.
    self.photoFileNameArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.sourcePath error:NULL];
    
    // Match dates with photos.
    self.photoDates = [[NSMutableArray alloc] initWithObjects:@"April 2, 2013", @"Jan. 19, 2013", @"April 5, 2013", @"Sept. 6, 2013", @"April 10, 2013", @"May 1, 2013", @"Aug. 28, 2013", nil];
}

- (void) setupCollectionView
{
    // Initialize Flow Layout.
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    // Initilaize collection view.
    self.photoCollectionView=[[PhotoCollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    // Set it as delegate and data source.
    [self.photoCollectionView setDataSource:self];
    [self.photoCollectionView setDelegate:self];
    
    // Register cell class and prepare each cell for re-use for efficiency.
    [self.photoCollectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    // Make background of view clear to keep that turquise we love so much.
    [self.photoCollectionView setBackgroundColor:[UIColor clearColor]];
    
    // Add collection view as subview to our root view.
    [self.view addSubview:self.photoCollectionView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // Create enough for each photo..
    return [self.photoFileNameArray count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    // Default is 1 but we will do it again for clarity.
    return 1;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Get image name and create a file extension for it.
    NSString* imageName = [self.photoFileNameArray objectAtIndex:indexPath.row];
    NSString *filename = [NSString stringWithFormat:@"%@/%@", self.sourcePath, imageName];
    
    // Set up our re-useable cell object which has an identifier "cellIdentifier".
    PhotoCollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    // Make cells white.
    cell.backgroundColor=[UIColor whiteColor];
    
    // Put our images into image views.
    UIImage* image = [UIImage imageWithContentsOfFile:filename];
    CGRect blurFrame = CGRectMake(0, cell.frame.size.height-50, cell.frame.size.width, cell.frame.size.height-200);
    image = [image applyLightEffectAtFrame:blurFrame];
    UIImageView* photoImageView = [[UIImageView alloc] initWithImage:image];
    
    CGRect nameFrame = CGRectMake(0, cell.frame.size.height-50, cell.frame.size.width/2, cell.frame.size.height-200);
    UILabel* photoLabel = [[UILabel alloc] initWithFrame:nameFrame];
    photoLabel.text = [imageName stringByDeletingPathExtension];
    photoLabel.textColor = [UIColor whiteColor];
    photoLabel.textAlignment = NSTextAlignmentCenter;
    [photoImageView addSubview:photoLabel];
    
    CGRect dateFrame = CGRectMake(cell.frame.size.width/2, cell.frame.size.height-50, cell.frame.size.width/2, cell.frame.size.height-200);
    UILabel* photoDate = [[UILabel alloc] initWithFrame:dateFrame];
    photoDate.text = [self.photoDates objectAtIndex:indexPath.row];
    photoDate.textColor = [UIColor whiteColor];
    photoDate.textAlignment = NSTextAlignmentCenter;
    [photoImageView addSubview:photoDate];
    
    // Add picture to cell as subview.
    [cell addSubview:photoImageView];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Make cell same width as application frame and 250 pixels tall.
    return CGSizeMake(self.view.frame.size.width, 250);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
